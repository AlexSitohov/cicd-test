import pytest
from fastapi import status
from fastapi.testclient import TestClient

import main


@pytest.fixture()
def client():
    yield TestClient(main.app)


def test_main(client):
    response = client.get('/')
    assert response.status_code == status.HTTP_200_OK
    assert response.json().get('message') == "Hello World"


def test_hello(client):
    name = "Lisa"
    response = client.get(f"/hello/{name}")
    assert response.status_code == status.HTTP_200_OK
    assert response.json().get('message') == f"Hello {name}"
