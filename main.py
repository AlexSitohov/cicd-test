from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}


@app.get("/cicd")
async def cicd():
    return {"message": "cicd is cool"}


@app.get("/hello_im_devops_now")
async def hello_im_devops_now():
    return {"message": "hello_im_devops_now!!!!!!!!!!!"}
